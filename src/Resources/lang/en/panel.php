<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'login' => 'Login',
    'logout' => 'Logout',
    'password' => 'Password',
    'remember_me' => 'Remember Me',
    'parent' => 'Parent Page',
    'no_parent' => 'No Parent Page',
    'order' => 'Order',
    'delete' => 'Delete',
    'close' => 'Close',
    'received' => 'Received Forms',
    'create_socialmedia'=> 'Create SocialMedia',
    'date' => 'Date',
    'status' => 'Status',
        'active' => 'Active',
        'passive' => 'Passive',
    'languages' => 'Languages',
    'view' => 'View',
    'slug' => 'Slug',
    'name' => 'Name',
    'email' => 'E-mail',
    'url' => 'URL',
    'content' => 'Content',
    'pages' => 'Pages',
    'create' => 'Create',
    'edit' => 'Edit',
    'save' => 'Save',
    'id' => 'ID',
    'action' => 'Action',
    'dashboard' => 'Dashboard',
    'default_language' => 'Default Language',
    'active_languages' => 'Active Languages',
    'update' => 'Update',
    'delete' => 'Delete',
    'add_pages' => 'Add Page',
    'dynamic' => 'Dynamic',
    'static' => 'Static',
    'type' => 'Type',
    'generating' => 'Generating for',
    'sub_pages' => 'Sub Pages',
    'message' => 'Message',
    'data' => 'Data',
    'show' => 'Show',
    'ip' => 'IP Address',
    'messages' => 'Messages',
    'ebulletins' => 'Ebulletins',
    'users' => 'Users',
    'roles' => 'Roles',
    'metas' => 'Metas',
    'redirects' => 'Redirects',
    'add' => 'Add',

    // FORMS MEGA THREAD
    'forms' => 'Forms',
    'form_create' => 'Create Form',
    'form_messages' => '":Form" Messages',
    'form_input' => 'Input',
    'form_value' => 'Value',

    'form_no_input' => 'User provided no input',
    'form_created' => 'Form Created',
    'form_saved' => 'Form Saved',
    'form_deleted' => ':Form_name deleted',
    'form_delete_modal_title' => 'Form will be deleted',
    'form_delete_modal_body' => 'Are you sure?',
    'form_delete_yes' => 'Yes, delete the form',
    'form_delete_no' => 'No, I want to keep this form',

    // Form create-edit input labels
    'form_name' => 'Name',
    'form_email' => 'Receiver Email',
    'form_slug' => 'Slug',
    'form_rules' => 'Rules',
    'form_error_messages' => 'Error Messages',

    // MESSAGES
    'messages_email' => 'Email',
    'messages_ip' => 'IP Address',
    'messages_show' => 'Show',
    'message_message' => 'Message',

    //Categories
    'categories' => 'Categories',
    'category_name' => 'Category Name',
    'select_file' => "Select a File",
    'confirm_text' => 'Are you sure?',

    //Menu
    'menu' => 'Menu',
    'items' => 'Items',

    //Slider
    'slider' => 'Slider',
];
